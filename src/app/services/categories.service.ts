import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from '../models/category';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  cache: Category[];

  constructor() {
    this.cache = [];
   }

  getCategories():Observable<Category[]> {

    return new Observable<Category[]>(res => {

      setTimeout(() => {
        let categories: Category[] = [
          {
            id: 1,
            image: '../../../assets/images/bebidas.png',
            name: 'bebidas'
          },
          {
            id: 2,
            image: '../../../assets/images/cafeteria.png',
            name: 'cafeteria'
          },
          {
            id: 3,
            image: '../../../assets/images/cucuruchos.png',
            name: 'cucuruchos'
          },
          {
            id: 4,
            image: '../../../assets/images/chocolate.png',
            name: 'chocolate'
          },
          {
            id: 5,
            image: '../../../assets/images/especialidades.png',
            name: 'especialidades'
          },
          {
            id: 6,
            image: '../../../assets/images/copahelada.png',
            name: 'copa helada'
          },
          {
            id: 7,
            image: '../../../assets/images/copadulce.png',
            name: 'copa dulce'
          },
          {
            id: 8,
            image: '../../../assets/images/promociones.png',
            name: 'promociones'
          },
        ];
        this.cache = categories;
        res.next(categories);
      }, 2000)
    });
  }

  getCache() {
    return this.cache;
  }
}
