import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SaleService {

  cacheCurrency: any[] = [];

  constructor(
    private http: HttpClient
  ) { 
    this.getCurrencies();
  }

  /**
   * Call the service to get the currency's value
   */
  getCurrencies() {
    this.http.get('https://www.dolarsi.com/api/api.php?type=valoresprincipales')
    .subscribe(result => {
      if (result && Array.isArray(result)) {
        this.cacheCurrency = [];
        for (let i = 0; i < result.length; i++) {
          this.cacheCurrency.push(result[i]);
        }
      }

    }, error => {
      console.log(error);
    })
  }

  /**
   * Calculate the final price
   * @param currency 
   * @param price 
   * @returns 
   */
  calculatePrice(currency: string, price: number): any {

    let formattedCurency: number = 0;
    this.cacheCurrency.forEach(curr => {
      if (curr.casa.nombre == currency) {
        if (Number.isNaN(curr.casa.venta)) {
          formattedCurency = Number.parseInt(curr.casa.venta.replace('.', '').replace(',','.'));
        } else {
          formattedCurency = Number.parseInt(curr.casa.venta);
        }
      }
    });

    return formattedCurency * price ;
  }
}
