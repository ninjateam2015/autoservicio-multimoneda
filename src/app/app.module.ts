import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { ProductsComponent } from './components/products/products.component';
import { ButtonComponent } from './gadgets/button/button.component';
import { ButtonImageComponent } from './gadgets/button-image/button-image.component';
import { SaleComponent } from './components/sale/sale.component';
import { DetailComponent } from './components/detail/detail.component';
import { HttpClientModule } from '@angular/common/http';
import { SaleService } from './services/sale.service';
import { LoadingComponent } from './gadgets/loading/loading.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    CategoriesComponent,
    ProductsComponent,
    ButtonComponent,
    ButtonImageComponent,
    SaleComponent,
    DetailComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    SaleService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
