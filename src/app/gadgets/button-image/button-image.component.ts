import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button-image',
  templateUrl: './button-image.component.html',
  styleUrls: ['./button-image.component.scss']
})
export class ButtonImageComponent {

  @Input() image: string;
  @Input() name: string;

  @Output() clickEvent = new EventEmitter<any>();

  constructor() {
    this.image = '';
    this.name = '';
   }

   onClick() {
    this.clickEvent.emit();
  }
}
