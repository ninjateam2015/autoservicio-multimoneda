import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Product } from 'src/app/models/product';
import { ProductsService } from 'src/app/services/products.service';
import { SaleService } from 'src/app/services/sale.service';

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.scss']
})
export class SaleComponent implements OnInit {

  saleForm: FormGroup;
  submitted: boolean;
  product?: Product;
  price: number;

  constructor(
    private router: Router,
    private location: Location,
    private fb: FormBuilder,
    private salesService: SaleService,
    private productService: ProductsService
  ) { 
      this.price = 0;
      const date = new Date();
      // Create Form
      this.saleForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.maxLength(50)]],
      cardNumber: ['', [Validators.required, Validators.minLength(16)]],
      expirationMonth: ['', [Validators.required, Validators.min(1), Validators.max(12), Validators.minLength(2)]],
      expirationYear: ['', [Validators.required, Validators.min(date.getFullYear()), Validators.max(date.getFullYear() + 50), Validators.minLength(4)]],
      cvc: ['', [Validators.required, Validators.minLength(3)]],
    });

    this.submitted = false;
  }

  ngOnInit() {
    // Check if a product was selected
    if (this.productService.getSelectedProduct()) {
      this.product = this.productService.getSelectedProduct()!;
      this.price = this.salesService.calculatePrice(this.product!.currency, this.product!.price);
    } else {
      this.router.navigateByUrl('products');
    }
  }

  back() {
    this.productService.clearSelectedProduct();
    this.location.back();
  }

  detail() {
    this.router.navigateByUrl('detail');
  }

  get fm() {
    return this.saleForm.controls;
  }

  completeSale() {
    this.submitted = true;
    if (this.saleForm.valid) {
      alert('Pago realizado');
    }
  }
}
