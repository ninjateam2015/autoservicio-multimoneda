export class Product {

    id: number = 0;
    name: string = '';
    image: string = '';
    description: string = '';
    price: number = 0;
    currency: string = '';
}
